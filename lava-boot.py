#!/usr/bin/python3

import argparse
import logging
import os
import requests
import sys
import time
from urllib.parse import urlparse
import xmlrpc.client
import yaml

from jinja2 import Environment, FileSystemLoader


###########
# Constants
###########
BOARDS = {
    "arndale" :         {"path": {"dtb": "arch/arm/boot/dts/exynos5250-arndale.dtb",
                                 "kernel": "arch/arm/boot/zImage"}},
    "beaglebone-black": {"path": {"dtb": "arch/arm/boot/dts/am335x-boneblack.dtb",
                                  "kernel": "arch/arm/boot/zImage"}},
    "juno":             {"path": {"dtb": "arch/arm64/boot/dts/arm/juno.dtb",
                                  "kernel": "arch/arm64/boot/Image"}},
    "pandaboard" :      {"path": {"dtb": "arch/arm/boot/dts/omap4-panda.dtb",
                                  "kernel": "arch/arm/boot/zImage"}},
}
LOG_FORMAT = "%(asctime)-15s %(levelname)7s %(message)s"

ERROR_INFRA = 2
ERROR_JOB = 1


##################
# Global variables
##################
LOG = logging.getLogger("root")


#########
# Helpers
#########
class RequestsTransport(xmlrpc.client.Transport):

    def __init__(self, scheme):
        super().__init__()
        self.scheme = scheme

    def request(self, host, handler, request_body, verbose=False):
        headers = {"User-Agent": self.user_agent,
                   "Content-Type": "text/xml",
                   "Accept-Encoding": "gzip"}
        url = "%s://%s%s" % (self.scheme, host, handler)
        try:
            response = None
            response = requests.post(url, data=request_body,
                                     headers=headers, timeout=20.0)
            response.raise_for_status()
            return self.parse_response(response)
        except requests.RequestException as exc:
            if response is None:
                raise xmlrpc.client.ProtocolError(url, 500, str(exc), "")
            else:
                raise xmlrpc.client.ProtocolError(url, response.status_code,
                                                  str(exc), response.headers)

    def parse_response(self, response):
        """
        Parse the xmlrpc response.
        """
        p, u = self.getparser()
        p.feed(response.text)
        p.close()
        return u.close()


def artifactorial(url, token, filename):
    try:
        ret = requests.post(url, data={"token": token},
                            files={"path": open(filename, "rb")})
    except OSError as exc:
        LOG.error("---> Unable to upload artifact")
        LOG.debug(exc)
        return None
    if ret.status_code != 200:
        LOG.error("---> Unable to upload artifact")
        LOG.debug(ret.text)
        return None
    return ret.text


def build_lava_job_uri(uri, job_id):
    """
    Build the url to the lava scheduler job
    """
    url = urlparse(uri)
    host = url.netloc
    if "@" in url.netloc:
        host = url.netloc.split("@")[1]

    return url.scheme + "://" + host + "/scheduler/job/%s" % job_id


def load_configuration(options):
    if options.config is not None:
        return yaml.load(options.config.read())
    else:
        env = os.environ
        try:
            return {"artifactorial": {"uri": env["ARTIFACTORIAL_URI"],
                                      "directory": env["ARTIFACTORIAL_DIR"],
                                      "token": env["ARTIFACTORIAL_TOKEN"]},
                    "lava": {"uri": env["LAVA_URI"]}}
        except KeyError as exc:
            LOG.error("%s is not defined", exc)
            return None


def render(device_type, context):
    try:
        # TODO: not going to work if changing the directories layout
        env = Environment(loader=FileSystemLoader(["share"]), trim_blocks=True)
        template = env.get_template("%s.jinja2" % device_type)
        return template.render(**context)
    except Exception as exc:
        LOG.exception(exc)
        return None


def setup_logging(options):
    """
    Setup the log handler and the log level
    """
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(logging.Formatter(LOG_FORMAT))
    LOG.addHandler(handler)
    if options.level == "DEBUG":
        LOG.setLevel(logging.DEBUG)
    elif options.level == "INFO":
        LOG.setLevel(logging.INFO)
    elif options.level == "WARNING":
        LOG.setLevel(logging.WARNING)
    else:
        LOG.setLevel(logging.ERROR)


################
# Main functions
################
def upload(options, config):
    # Post artifacts
    artifactorial_url = "%s/artifacts%s/" % (config["artifactorial"]["uri"],
                                             config["artifactorial"]["directory"])
    LOG.debug("-> artifactorial: %s", artifactorial_url)

    ctx = {}
    for key, path in BOARDS[options.board]["path"].items():
        LOG.debug("--> uploading %s as %s", path, key)
        url = artifactorial(artifactorial_url, config["artifactorial"]["token"],
                            os.path.join(options.linux, path))
        if url is None:
            return None
        # HACK: should be removed when the lab proxy is fixed
        ctx[key] = url.replace("http://", "https://")

    return ctx


def test(options, config, ctx):
    # Create the xmlrpc proxy
    transport = RequestsTransport(urlparse(config["lava"]["uri"]).scheme)
    proxy = xmlrpc.client.ServerProxy(config["lava"]["uri"], allow_none=True,
                                      transport=transport)

    # TODO: add an handler for Ctrl+C
    for i in range(0, options.retries):
        LOG.info("Try n°%d", i)
        LOG.info("-> submitting %d jobs", options.parallel)
        job_ids = []
        for j in range(0, options.parallel):
            # Create the job definition
            ctx["job_name"] = "%s [%d.%d/%d]" % (options.name, i, j, options.parallel)
            definition = render(options.board, ctx)
            if definition is None:
                LOG.error("-> unable to create the job definition")
                return 1

            job_id = proxy.scheduler.jobs.submit(definition)
            LOG.info("--> %s", job_id)
            job_ids.append(job_id)

        LOG.info("-> waiting for:")
        jobs = {}
        for job_id in job_ids:
            LOG.info("--> %s", build_lava_job_uri(config["lava"]["uri"], job_id))
            while True:
                job = proxy.scheduler.jobs.show(job_id)
                if job["state"] == "Finished":
                    break

                LOG.debug("---> %s", job["state"])
                time.sleep(10)
                job = proxy.scheduler.jobs.show(job_id)

            LOG.info("--> Result: %s", job["health"])
            jobs[job_id] = job

        # Count the success/failures
        for job_id in jobs:
            if jobs[job_id]["health"] == "Complete":
                return 0

    # TODO: understand why this is failing
    return 1


#############
# Entry point
#############
def main():
    # Setup the parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--level", type=str, default="DEBUG",
                        choices=["DEBUG", "ERROR", "INFO", "WARN"],
                        help="Log level, default to INFO")
    parser.add_argument("--config", type=argparse.FileType("r"),
                        default=None, help="configuration file")
    parser.add_argument("--board", type=str, required=True, help="device type")
    parser.add_argument("--name", type=str, required=True, help="job name")
    parser.add_argument("--parallel", type=int, default=2, help="number of jobs to run in parallel")
    parser.add_argument("--retries", type=int, default=3, help="number of retries to run")

    parser.add_argument("linux", type=str, help="path to the linux build")

    # Parse the command line
    options = parser.parse_args()

    # Setup logging
    setup_logging(options)

    # Load the configuration
    config = load_configuration(options)
    if config is None:
        return ERROR_JOB

    # Upload artifacts
    ctx = upload(options, config)
    if ctx is None:
        return ERROR_INFRA

    return test(options, config, ctx)


if __name__ == "__main__":
    sys.exit(main())
